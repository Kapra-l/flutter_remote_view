import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:module3navigation/generated/i18n.dart';

import 'Module6_WebView/MyMapViewWidget.dart';
import 'Module6_WebView/MyVideoPlayerWidget.dart';
import 'Module6_WebView/MyWebViewWidget.dart';

/*

One main screen with 3 tabs:
1st - WebView with any web-page loaded
2nd - MapView with one market (random location)
3rd - VideoPlayer with any video in it.

 */

void main() => runApp(MyApp());

class MyApp extends StatefulWidget {
  @override
  MyAppState createState() => MyAppState();
}

class MyAppState extends State<MyApp> {
  final i18n = I18n.delegate;

  @override
  void initState() {
    super.initState();
    I18n.onLocaleChanged = onLocaleChange;
  }

  void onLocaleChange(Locale locale) {
    setState(() {
      I18n.locale = locale;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      localizationsDelegates: [
        I18n.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalMaterialLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate
      ],
      supportedLocales: i18n.supportedLocales,
      home: DefaultTabController(
        length: 3,
        child: Scaffold(
          appBar: AppBar(
            title: Text("Module 6"),
            bottom: TabBar(
              tabs: [
                Tab(icon: Icon(Icons.web)),
                Tab(icon: Icon(Icons.map)),
                Tab(icon: Icon(Icons.personal_video)),
              ],
            ),
          ),
          body: TabBarView(
            children: [
              MyWebViewWidget(),
              MyMapViewWidget(),
              MyVideoPlayerWidget()
            ],
          ),
        ),
      ),
    );
  }
}

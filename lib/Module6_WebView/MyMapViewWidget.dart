import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:module3navigation/generated/i18n.dart';

class MyMapViewWidget extends StatefulWidget {

  @override
  MyMapViewState createState() => MyMapViewState();
}

class MyMapViewState extends State<MyMapViewWidget> {
  Completer<GoogleMapController> _controller = Completer();

  static const LatLng _mapPosition = const LatLng(53.876122, 27.629930);

  void _onMapCreated(GoogleMapController controller) {
    _controller.complete(controller);
  }
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(I18n.of(context).titlesMap_view)
      ),
      body: GoogleMap(
          onMapCreated: _onMapCreated,
          initialCameraPosition: CameraPosition(
            target: _mapPosition,
            zoom: 11.0,
          ),
          markers: Set<Marker>.of([
            Marker (
              markerId: MarkerId(_mapPosition.toString()),
              position: _mapPosition,
              infoWindow: InfoWindow(
                title: I18n.of(context).markerTitle,
                snippet: I18n.of(context).markerSnippet,
              ),
              icon: BitmapDescriptor.defaultMarker,
            )
          ]),
        ),
    );
  }
}
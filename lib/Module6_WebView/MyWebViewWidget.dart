import 'dart:async';

import 'package:flutter/material.dart';
import 'package:module3navigation/generated/i18n.dart';
import 'package:webview_flutter/webview_flutter.dart';

class MyWebViewWidget extends StatefulWidget {

  @override
  MyWebViewState createState() => MyWebViewState();
}

class MyWebViewState extends State<MyWebViewWidget> {
  Completer<WebViewController> _controller = Completer<WebViewController>();
  
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(I18n.of(context).titlesWeb_view)
      ),
      body: WebView(
        initialUrl: 'https://medium.com/flutter/the-power-of-webviews-in-flutter-a56234b57df2',
        onWebViewCreated: (WebViewController webViewController) {
          _controller.complete(webViewController);
        },
      ),
    );
  }
}